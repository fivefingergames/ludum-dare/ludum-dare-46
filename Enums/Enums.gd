extends Node

class_name LDEnums

enum ActionType {
	PICK,
	BANDAGE,
	MORPHEUM,
	ADRENALINE
}

enum Sounds {
	GAMEINTRO
	GAME
	MAINMENU
	MAINMENUINTRO
	BANDAGE
	BUTTON1
	BUTTON2
	BUTTON3
	BUTTON4
	BUTTON5
	CALLOUT1
	CALLOUT2
	CALLOUT3
	DRAGGING
	FOOTSTEPS
	HIT1
	HIT2
	HIT3
	INTOBED
	MEDIC1
	MEDIC2
	MEDIC3
	MORPHIUM1
	MORPHIUM2
	MORPHIUM3
	NEXTWAVE
	OUTOFBED
	SHOT
}
