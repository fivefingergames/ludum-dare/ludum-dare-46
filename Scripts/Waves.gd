extends Resource
class_name Waves

export (Array, Dictionary) var waves = [];

class Wave:
	var name: String ="";
	var number: int = 0;
	var enemies:= 10;
	var bullet_speed:= 2
