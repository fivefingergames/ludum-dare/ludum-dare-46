tool
extends StaticBody2D

export var enabled := true setget _set_enabled;

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	# set_enabled(enabled)
	pass
	
func _set_enabled(value):
	enabled = value;
	$CollisionShape2D.disabled = !enabled;
	$Sprite.visible = enabled;
