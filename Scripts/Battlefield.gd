extends Node2D
class_name Battlefield

signal spawn_bullet(direction, bullet_position)
signal spawn_sniper_bullet(bullet_position)
export (PackedScene) var Bullet;

onready var bulletTimer: Timer = $BulletTimer;
onready var sniperTimer: Timer = $SniperTimer;
onready var bulletPosition: PathFollow2D = $BulletPath/BulletPosition;

onready var obstacles = [
	$Obstacles/Obstacle,
	$Obstacles/Obstacle2,
	$Obstacles/Obstacle3,
	$Obstacles/Obstacle4,
	$Obstacles/Obstacle5,
	$Obstacles/Obstacle6,
	$Obstacles/Obstacle7,
	$Obstacles/Obstacle8,
	$Obstacles/Obstacle9,
	$Obstacles/Obstacle10,
	$Obstacles/Obstacle11
]
var obstacles_bought = 2;


func start():
	bulletTimer.start();
	sniperTimer.start();
	spawn_sniper_bullet();
	
func stop():
	bulletTimer.stop();
	sniperTimer.stop();
	
func set_bullet_timer(wait_time):
	bulletTimer.wait_time = wait_time;
	
func set_sniper_timer(wait_time):
	sniperTimer.wait_time = wait_time;

func _on_BulletTimer_timeout():
	bulletPosition.unit_offset = randf()
	var direction := bulletPosition.rotation
	direction += PI / 2
	direction += rand_range(-PI/4, PI/4)
	emit_signal("spawn_bullet", direction, bulletPosition.global_position);

func _on_SniperTimer_timeout():
	spawn_sniper_bullet();
	
func spawn_sniper_bullet():
	bulletPosition.unit_offset = randf()
	emit_signal("spawn_sniper_bullet", bulletPosition.global_position);
	
func add_obstacle():
	obstacles[obstacles_bought].enabled = true;
	obstacles_bought += 1;

func restart():
	obstacles_bought = 2;
	for obstacle in obstacles:
		obstacle.enabled = false;
	$Obstacles/Obstacle.enabled = true;
	$Obstacles/Obstacle2.enabled = true;
