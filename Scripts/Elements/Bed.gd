extends StaticBody2D

const HEALING_TIME = 0.2;
const HEALING_TIME_UPGRADED = 0.1;

var soldier: Soldier
export var enabled := true setget _set_enabled;

func _ready():
	pass

func interact(player: Player, action_type: int):
	if action_type == LDEnums.ActionType.PICK:
		var interactible = player.get_carried_interactible()
		if interactible != null:	
			if !(interactible is Soldier):
				return
				
			soldier = interactible as Soldier
			AudioPlayer.play_sounds(LDEnums.Sounds.INTOBED)
			soldier.is_carried = false
			soldier.global_position = $SoldierPosition.global_position
			soldier.is_treated = true
		# warning-ignore:return_value_discarded
			soldier.connect("restored", self, "on_soldier_restored")
			soldier.healingTimer.wait_time = player.bed_time;
			
			player.set_carried_interactible(null)
			player.is_interacting = false
		elif soldier != null:
			soldier.is_treated = false
			soldier.disconnect("restored", self, "on_soldier_restored")
			soldier.interact(player, LDEnums.ActionType.PICK)
			soldier = null
			AudioPlayer.play_sounds(LDEnums.Sounds.OUTOFBED)
	elif action_type == LDEnums.ActionType.MORPHEUM:
		if !soldier.wounded:
			return
			
		soldier.health = int(clamp(soldier.health + soldier.healthBar.max_value * player.morphium_heal_factor, soldier.health, soldier.healthBar.max_value-1))
		player.morphium -= 1
	
	
func end_interaction(_player: Player):
	pass

func can_interact(player: Player, action_type: int = LDEnums.ActionType.PICK)->bool:
	if action_type == LDEnums.ActionType.PICK:
		var possible = false
		possible = _bed_is_free(player) || _bed_occupied(player)
		return possible
	elif action_type == LDEnums.ActionType.MORPHEUM:
		return _bed_occupied(player)
		
	return false
	
func on_soldier_restored(_solder: Soldier):
	soldier.global_position = $RecoverPosition.global_position
	soldier.is_treated = false
	soldier.disconnect("restored", self, "on_soldier_restored")
	soldier = null
	
	
func _set_enabled(value):
	enabled = value;
	$CollisionShape2D.disabled = !enabled;
	$Sprite.visible = enabled;
	
func _bed_is_free(player: Player)->bool:
	print("bed is free")
	return soldier == null && player.get_carried_interactible() != null
	
func _bed_occupied(player: Player)->bool:
	print("bed occupied")
	return soldier != null && player.get_carried_interactible() == null
