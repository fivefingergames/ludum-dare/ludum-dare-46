extends Node2D

onready var Bullets: Node = $Bullets
onready var battlefield: Battlefield = $Battlefield
onready var sickBay: SickBay = $SickBay
onready var ui: Ui = $UI;
onready var player: Player = $Player;
onready var gurney: Node2D = $Gurney;
onready var randomSoldierPositionLeft: PathFollow2D = $RandomSoldierPositionLeft/Position;
onready var randomSoldierPositionRight: PathFollow2D = $RandomSoldierPositionRight/Position;
export (PackedScene) var Bullet;
export (PackedScene) var Soldier;
export (Resource) var SoldierNames;
export (Resource) var Waves;
export var current_wave:= 0;
export var debug:= false;

export (int) var money = 100 setget set_money;
var screen_size: Vector2;
var remaining_soldiers = 0;
var restored_soldiers = 0;
var soldiers = []
var next_target: Node2D = null;

func _init():
	randomize()

func _ready():
	get_viewport().audio_listener_enable_2d = true
	AudioPlayer.play_music(LDEnums.Sounds.MAINMENUINTRO)
	screen_size = get_viewport_rect().size;
	refresh_hud();
	player.gurney = gurney;
	
func _unhandled_input(event):
	if !debug:
		return;
	if !event is InputEventMouseButton:
		return;
	if !event.is_pressed():
		return;
	spawn_bullet(PI/2, event.position)

func _on_Battlefield_spawn_bullet(bullet_rotation: float, bullet_position: Vector2):
	spawn_bullet(bullet_rotation, bullet_position);
	
func spawn_bullet(bullet_rotation: float, bullet_position: Vector2):
	AudioPlayer.play_sounds(LDEnums.Sounds.SHOT, -14)
	var bullet: Node2D = Bullet.instance();
	bullet.rotation = bullet_rotation;
	bullet.position = bullet_position;
	bullet.linear_velocity = bullet.linear_velocity.rotated(bullet_rotation)
	Bullets.add_child(bullet)
	

func _on_Battlefield_spawn_sniper_bullet(bullet_position):
	spawn_sniper_bullet(bullet_position);
	
func spawn_sniper_bullet(bullet_position: Vector2):
	var target_offset = Vector2(0,6);
	AudioPlayer.play_sounds(LDEnums.Sounds.SHOT, -14)
	var bullet: Node2D = Bullet.instance();
	Bullets.add_child(bullet)
	bullet.global_position = bullet_position;
	
	var target = next_target;
	next_target = null;
	if target == null:
		var targets = []
		for soldier in soldiers:
			if !soldier.killed and !soldier.restored and !soldier.is_treated:
				targets.append(soldier);
				targets.append(soldier);
		targets.append(player);
		target = targets[randi()%targets.size()];
		if target.is_in_group("Soldiers"):
			target.wait_for_kill = true;
			target.set_animation_parameters(false, false, true)
			bullet.connect("hit", self, "_on_SniperBullet_missed", [target]);
		print("Sniper", target.get_path(), targets);
	var bullet_rotation;
	
	$SniperLine.points = [bullet.global_position, target.global_position];
	bullet_rotation = ((target.global_position+target_offset) - bullet.global_position).angle();
	
	bullet.rotation = bullet_rotation;
	bullet.linear_velocity = bullet.linear_velocity.rotated(bullet_rotation)

func _on_SniperBullet_missed(soldier):
	soldier.wait_for_kill = false;
	
func _on_UI_start_game():
	AudioPlayer.play_sounds(LDEnums.Sounds.MEDIC3)
	AudioPlayer.play_music(LDEnums.Sounds.GAMEINTRO, 4)
	
	prepare_wave();
	ui.hide_main_menu();
	var func_ref = funcref(self, "get_simple_path");
	for soldier in soldiers:
		soldier.setup(func_ref);
	ui.show_hud()
	refresh_hud();
	
func get_simple_path(start_position:Vector2, end_position:Vector2) -> PoolVector2Array:
	return $Navigation2D.get_simple_path(start_position, end_position, false);
	
func _set_money(value):
	money = value;
	refresh_hud();


func _on_UI_bought(upgrade):
	self.money -= upgrade.price;
	match upgrade.name:
		"BuyBed":
			sickBay.add_bed();
		"BuyMorphium":
			player.morphium += 1;
		"BuyObstacle":
			battlefield.add_obstacle();
		"UpgradeBed":
			player.upgraded_level_beds += 1;
		"UpgradeBoots":
			player.upgraded_level_boots += 1;
		"UpgradeMorphium":
			player.upgraded_level_morphium += 1;
	refresh_hud();

func refresh_hud():
	ui.refresh_hud(money, player.morphium, remaining_soldiers);
	
func _on_Player_killed():
	ui.show_game_over()

func _on_Player_adrenalin_changed(_value):
	refresh_hud();
	
func _on_Player_morphium_changed(_value):
	refresh_hud();

func _on_UI_restart_game():
	player.restart();
	battlefield.restart();
	clear_soldiers();
	current_wave = 0;
	self.money = 100;
	refresh_hud();
	battlefield.stop();
	AudioPlayer.play_music(LDEnums.Sounds.MAINMENUINTRO, 4)

func clear_soldiers():
	for soldier in soldiers:
		$Soldiers.remove_child(soldier);
	soldiers.clear()
	remaining_soldiers = 0;

func spawn_soldier():
	var soldier: Soldier = Soldier.instance();
	if current_wave == 0: #fixed Placement for first 2 soldiers
		if remaining_soldiers == 0:
			#first soldier, will be placed at front line and easily killed
			soldier.position = Vector2(screen_size.x * randf(), ((screen_size.y / 2) * randf() + 64));
			soldier.wait_for_kill = true;
			next_target = soldier;
		else:
			soldier.position = Vector2(screen_size.x*randf(), (screen_size.y * randf()) + 64);
	else:
		var soldier_position: PathFollow2D;
		if randi() % 2 == 0:
			soldier_position = randomSoldierPositionLeft;
		else:
			soldier_position = randomSoldierPositionRight;
		soldier_position.unit_offset = randf();
		soldier.position = soldier_position.global_position; 
	
	soldier.soldier_name = SoldierNames.names[randi() % SoldierNames.names.size()]
	
	
	soldiers.append(soldier);
	$Soldiers.add_child(soldier);
# warning-ignore:return_value_discarded
	soldier.connect("killed", self, "_on_Soldier_killed", [], CONNECT_ONESHOT);
# warning-ignore:return_value_discarded
	soldier.connect("restored", self, "_on_Soldier_restored", [], CONNECT_ONESHOT);
	remaining_soldiers += 1;
	var func_ref = funcref(self, "get_simple_path");
	for soldier in soldiers:
		soldier.setup(func_ref);


func _on_Soldier_killed(soldier: Soldier):
	soldier.disconnect("restored", self, "_on_Soldier_restored");
	remaining_soldiers -= 1;
	refresh_hud();
	
	if restored_soldiers == 0 and remaining_soldiers == 0:
		ui.show_fail_to_save()
		return
	
	if remaining_soldiers <= 0:
		prepare_next_wave();


func _on_Soldier_restored(soldier: Soldier):
	restored_soldiers += 1;
	soldier.disconnect("killed", self, "_on_Soldier_killed");
	remaining_soldiers -= 1;
	self.money += 100;
	refresh_hud();
	
	if remaining_soldiers <= 0:
		prepare_next_wave();
		
func prepare_next_wave():
	restored_soldiers = 0;
	battlefield.stop();
	restored_soldiers = 0
	ui.show_buy_menu();

func next_wave():
	current_wave += 1;
	if current_wave >= Waves.waves.size():
		current_wave = Waves.waves.size() - 1;
	prepare_wave();
	
func prepare_wave():
	var wave = Waves.waves[current_wave];
	if wave.promotion:
		ui.show_promotion_menu();
		yield(ui, "promoted");
	
	
	for i in range(soldiers.size()-1, -1, -1):
		var soldier = soldiers[i] as Soldier;
		if soldier.killed:
			soldiers.remove(i);
			soldier.queue_free();
		else:
			soldier.veteran_level += 1;
			soldier.ready_for_wave();
			if !soldier.is_connected("killed", self, "_on_Soldier_killed"):
				soldier.connect("killed", self, "_on_Soldier_killed", [], CONNECT_ONESHOT);
			if !soldier.is_connected("restored", self, "_on_Soldier_restored"):
				soldier.connect("restored", self, "_on_Soldier_restored", [], CONNECT_ONESHOT);
			remaining_soldiers += 1
		
	for i in wave.soldiers - soldiers.size():
		spawn_soldier()
	battlefield.set_bullet_timer(wave.bullet_speed);
	battlefield.set_sniper_timer(wave.sniper_speed);
	refresh_hud()
	battlefield.start();


func _on_UI_next_wave_pressed():
	ui.hide_buy_menu();
	next_wave();
func set_money(value: int):
	money = value;
	if ui:
		ui.money = value;
