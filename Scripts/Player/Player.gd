extends KinematicBody2D

class_name Player

signal morphium_changed(value);
signal killed();

const ACCELERATION := 500
const MAX_SPEED_BASE := 75
const MORPHIUM_HEAL := 0.5
const BED_HEALING := 0.2
const FRICTION := 500

var velocity := Vector2.ZERO
var health := 100 setget _set_health;
var interaction_progress := 0 setget _set_interaction_progress
onready var healthBar: TextureProgress = $HealthBarHolder/HealthBar;
onready var interactBar: TextureProgress = $InteractiveProgressHolder/InteractProgress;
onready var notification: Label = $Notification;
onready var animationTree: AnimationTree = $AnimationTree;
onready var playback = animationTree.get("parameters/playback")
var transitioning = false;
var killed := false
var is_interacting := false
var action_progressing := false setget _set_action_progressing
var interactible_body: Node2D
var carried_interactible: Node2D
var gurney: Node2D;
var morphium_heal_factor setget ,_get_morphium_heal_factor;
var speed setget ,_get_speed;
var bed_time setget ,_get_bed_time;

export var morphium := 1 setget _set_morphium;
export var upgraded_level_boots := 0;
export var upgraded_level_morphium := 0;
export var upgraded_level_beds := 0;

func _ready():
	pass

func restart():
	morphium = 1;
	upgraded_level_boots = 0;
	upgraded_level_morphium = 0;
	upgraded_level_beds = 0;
	self.health = 100;
	killed = false;
	is_interacting = false;
	playback.start("Idle");
	
	
func _physics_process(delta: float):
	
	if action_progressing:
		if	interactBar.value < interactBar.max_value:
			interactBar.value += delta
		else:
			self.action_progressing = false
			interactBar.value = 0.0
			
	
	if killed:
		return
		
	handle_movement(delta)
		
	if Input.is_action_just_pressed("ui_select"):
		if !is_interacting || (is_interacting && carried_interactible != null && carried_interactible != interactible_body):
			interact_with(interactible_body, LDEnums.ActionType.PICK)
		else:
			end_interaction(interactible_body)
			
	if Input.is_action_just_pressed("ui_bandage"):
		if interactible_body != null && interactible_body.can_interact(self, LDEnums.ActionType.BANDAGE):
			interactBar.value = 0.0
			self.action_progressing = true
			interact_with(interactible_body, LDEnums.ActionType.BANDAGE)
	
	if Input.is_action_just_pressed("ui_morpheum"):
		if interactible_body != null && morphium > 0:
			interact_with(interactible_body, LDEnums.ActionType.MORPHEUM)
			

func hit_by_bullet():
	healthBar.visible = true;
	self.health -= 33
	AudioPlayer.play_sounds(randi() % 3 + LDEnums.Sounds.HIT1, 4)
	
func _set_health(newvalue):
	health = newvalue;
	healthBar.value = health;
	killed = health <= 0;
	if killed:
		interactible_body = null
		carried_interactible = null
		emit_signal("killed")
		playback.start("Death")

func handle_movement(delta: float):
	if action_progressing:
		return	
	if transitioning:
		return;
	var input_vector := Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	input_vector = input_vector.normalized()
	if input_vector != Vector2.ZERO:
		set_animation_vector(input_vector);
	
	if input_vector != Vector2.ZERO:
		velocity = velocity.move_toward(input_vector * self.speed, ACCELERATION * delta)
	else:
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
	
	var is_carrying = carried_interactible != null;
	var idle = velocity == Vector2.ZERO && !is_carrying;
	var moving = velocity != Vector2.ZERO && !is_carrying;
	set_animation_params(is_carrying, idle, moving, velocity);
	
	velocity = move_and_slide(velocity)
	gurney.global_position = self.global_position;

func set_animation_vector(vector: Vector2):
	gurney.set_animation_vector(vector);
	animationTree.set("parameters/Carrying/blend_position", vector);
	animationTree.set("parameters/Idle/blend_position", vector);
	animationTree.set("parameters/Locomotion/blend_position", vector);
	animationTree.set("parameters/Death/blend_position", vector);
	
func set_animation_params(is_carrying, idle, moving, input_velocity):
	gurney.set_animation_params(is_carrying, idle, moving, input_velocity);
	animationTree.set("parameters/conditions/is_carrying", is_carrying);
	animationTree.set("parameters/conditions/idle", idle);
	animationTree.set("parameters/conditions/moving", moving);

func _on_Area2D_body_entered(body):
	if is_interactible(body) && body.can_interact(self):
		interactible_body = body
		
		notification.visible = true


func _on_Area2D_body_exited(body: Node2D):
	if !is_interactible(body):
		return;
	if body == interactible_body && !is_interacting:
		notification.visible = false
		interactible_body = null
	elif carried_interactible != null && carried_interactible != interactible_body:
		interactible_body = carried_interactible
		detect_interacts();
		
func interact_with(body: Node2D, action_type: int = LDEnums.ActionType.PICK):
	if !is_interactible(body):
		return

	if !body.can_interact(self, action_type):
		return
		
	body.interact(self, action_type);
	detect_interacts();
	
	notification.visible = false

func end_interaction(body: Node2D):
	if !is_interactible(body):
		return
	
	body.end_interaction(self);

	is_interacting = false
	interactible_body = null
	detect_interacts();
	
func is_interactible(body: Node2D)->bool:		
	return body && body.is_in_group("Interactibles")
	
func get_carry_position()->Node2D:
	return gurney.get_carry_position();
	
func set_carried_interactible(interactible: Node2D):
	if interactible == null:
		carried_interactible.disconnect("killed", self, "soldier_died")
		
	carried_interactible = interactible
		
	
func get_carried_interactible()->Node2D: 
	return	carried_interactible
	
func _set_morphium(value):
	morphium = value;
	emit_signal("morphium_changed", value)

func _set_interaction_progress(new_value: int):
	interaction_progress = new_value
	
func _set_action_progressing(new_value: bool):
	action_progressing = new_value
	interactBar.visible = action_progressing


func _on_GameCam_transition_started():
	transitioning = true;
	velocity = Vector2.ZERO;


func _on_GameCam_transition_ended():
	transitioning = false;

func soldier_died(_solider):
	if carried_interactible:
		if interactible_body == carried_interactible:
			interactible_body = null
			
		carried_interactible = null
		is_interacting = false
	
func _get_morphium_heal_factor():
	return MORPHIUM_HEAL * pow(1.1, upgraded_level_morphium);
func _get_speed():
	return MAX_SPEED_BASE * pow(1.1, upgraded_level_boots);
func _get_bed_time():
	return BED_HEALING * pow(0.9, upgraded_level_beds);
	
func detect_interacts():
	$Area2D/CollisionShape2D.set_deferred("disabled", true)
	yield(get_tree().create_timer(0.01), "timeout")
	$Area2D/CollisionShape2D.set_deferred("disabled", false)
