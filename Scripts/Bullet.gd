extends RigidBody2D

class_name Bullet

signal hit();

func _on_Bullet_body_entered(body: Node):
	if body.is_in_group("BulletColliders"):
		if body.has_method("hit_by_bullet"):
			body.hit_by_bullet();
		self.queue_free()
		emit_signal("hit");

