extends Node2D

var last_used_player: int

onready var audio_players = [
	$MainMenu1,
	$MainMenu2,
	$MainMenu3,
	$MainMenu4,
	$MainMenu5,
	$MainMenu6,
	$MainMenu7,
	$MainMenu8,
	$MainMenu9,
	$MainMenu10
];

onready var backgroundPlayer = $Background;
export (Array, AudioStream) var sounds;
var backgroundSound;

func play_sounds(sound_id: int, volume: int = 0):	
	var audio_player = audio_players[last_used_player]
	audio_player.stream = sounds[sound_id]
	audio_player.volume_db = volume
	audio_player.play()
	last_used_player += 1
	last_used_player = last_used_player % audio_players.size()
	
func play_music(sound_id: int, volume: int = 0):
	backgroundSound = sound_id;
	backgroundPlayer.volume_db = volume
	backgroundPlayer.stream = sounds[sound_id]
	backgroundPlayer.play()


func _on_Background_finished():
	match backgroundSound:
		LDEnums.Sounds.GAMEINTRO:
			play_music(LDEnums.Sounds.GAME);
		LDEnums.Sounds.GAME:
			play_music(LDEnums.Sounds.GAME);
		LDEnums.Sounds.MAINMENUINTRO:
			play_music(LDEnums.Sounds.MAINMENU);
		LDEnums.Sounds.MAINMENU:
			play_music(LDEnums.Sounds.MAINMENU);
