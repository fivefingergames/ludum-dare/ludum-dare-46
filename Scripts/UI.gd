extends Node

class_name Ui

signal start_game();
signal restart_game();
signal bought(upgrade);
signal next_wave_pressed();
signal promoted();

onready var mainMenu: Container = $MainMenu;
onready var buyUpgrade: Container = $BuyUpgrades;
onready var hud: Container = $HUD;
onready var gameOver: Container = $GameOver;
onready var failToSave: Container = $FailToSave;
var Upgrades = load("res://Resources/Upgrades.tres");
var money := 0 setget _set_money;
var obstacles_bought = 0;
var beds_bought = 0;

func _on_Button_pressed():
	AudioPlayer.play_sounds(LDEnums.Sounds.BUTTON5)

func _on_StartGameButton_pressed():
	AudioPlayer.play_sounds(LDEnums.Sounds.BUTTON5)
	emit_signal("start_game")

func _on_ExitGameButton_pressed():
	AudioPlayer.play_sounds(LDEnums.Sounds.BUTTON5)
	get_tree().quit();
	
func show_main_menu():
	mainMenu.show();
	
func hide_main_menu():
	mainMenu.hide();
	
func show_game_over():
	gameOver.show();
	
func show_fail_to_save():
	failToSave.show();
	
func hide_game_over():
	gameOver.hide();
	
func hide_fail_to_save():
	failToSave.hide();

func show_buy_menu():
	print("buy!")
	buyUpgrade.show()
	
func hide_buy_menu():
	buyUpgrade.hide()

func show_hud():
	hud.show();
	
func hide_hud():
	hud.hide();
	
func refresh_hud(newMoney: int, morphium: int, remaining_soldiers: int):
	self.money = newMoney;
	$HUD/HUDPanel/MoneyLabel.text = "Honor: "+str(money);
	$HUD/HUDPanel/MorphiumLabel.text = "Morphium: "+str(morphium);
	$HUD/HUDPanel/SoldiersLabel.text = "Saved Soldiers: "+str(remaining_soldiers);


func _on_ExitBuyMenu_pressed():
	AudioPlayer.play_sounds(LDEnums.Sounds.NEXTWAVE)
	emit_signal("next_wave_pressed");
	
func buy(name: String):
	AudioPlayer.play_sounds(LDEnums.Sounds.BUTTON5)
	var upgrade = Upgrades.get_upgrade(name);
	emit_signal("bought", upgrade);
	match upgrade.name:
		"BuyBed":
			beds_bought += 1;
		"BuyObstacle":
			obstacles_bought += 1;
		"UpgradeBed":
			hide_promition_menu();
			emit_signal("promoted")
		"UpgradeBoots":
			hide_promition_menu();
			emit_signal("promoted")
		"UpgradeMorphium":
			hide_promition_menu();
			emit_signal("promoted")
	refresh_buy_menu();

func hide_promition_menu():
	$PromitionMenu.hide();
	
func show_promotion_menu():
	$PromitionMenu.show();

func _set_money(value: int):
	money = value;
	refresh_buy_menu()

func refresh_buy_menu():
	var money_50 = money < 50;
#	var money_150 = money < 150;
	var money_300 = money < 300;
	
	$BuyUpgrades/Panel/BuyMorphium.disabled = money_50;
	$BuyUpgrades/Panel/BuyBed.disabled = money_300 or beds_bought >= 4;
	$BuyUpgrades/Panel/BuyObstacle.disabled = money_300 or obstacles_bought >= 5;
	
func _on_GameOverButton_pressed():
	AudioPlayer.play_sounds(LDEnums.Sounds.BUTTON5)
	reset_data();
	hide_game_over();
	show_main_menu();
	emit_signal("restart_game");


func _on_LeaveGame_pressed():
	reset_data();
	hide_fail_to_save();
	show_main_menu();
	emit_signal("restart_game");
	
func reset_data():
	money = 100;
	obstacles_bought = 0;
	beds_bought = 0;
	refresh_buy_menu();
