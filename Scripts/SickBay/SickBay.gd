extends Node2D
class_name SickBay

onready var beds = [
	$Beds/Bed,
	$Beds/Bed2,
	$Beds/Bed3,
	$Beds/Bed4,
	$Beds/Bed5,
	$Beds/Bed6
]
var beds_bought = 2;

func add_bed():
	beds[beds_bought].enabled = true;
	beds_bought += 1;

