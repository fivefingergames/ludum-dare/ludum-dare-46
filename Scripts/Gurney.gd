extends Node2D

onready var animationTree: AnimationTree = $AnimationTree;
onready var playback = animationTree.get("parameters/playback")
onready var dirtParticles:Particles2D = $CarryInteractible/DirtParticles

func set_animation_vector(vector: Vector2):
	animationTree.set("parameters/Carrying/blend_position", vector);
	animationTree.set("parameters/Idle/blend_position", vector);
	animationTree.set("parameters/Locomotion/blend_position", vector);
	animationTree.set("parameters/Death/blend_position", vector);
	
func set_animation_params(is_carrying, idle, moving, input_velocity):
	animationTree.set("parameters/conditions/is_carrying", is_carrying);
	animationTree.set("parameters/conditions/idle", idle);
	animationTree.set("parameters/conditions/moving", moving);
	self.visible = is_carrying;
	if input_velocity == Vector2.ZERO:
		dirtParticles.emitting = false;
	else:
		dirtParticles.emitting = true;

func get_carry_position():
	return $CarryInteractible as Node2D
