extends Node2D

signal transition_started();
signal transition_ended();

onready var camera: Camera2D = $Camera2D;
onready var positionBattlefield: Position2D = $PositionBattlefield;
onready var positionSickbay: Position2D = $PositionSickbay;
onready var tween: Tween = $Tween;
var is_on_battlefield = true;

func enter_battlefield(body):
	if is_on_battlefield:
		return;
	if body is Player:
# warning-ignore:return_value_discarded
		tween.interpolate_property(camera, "position", null, positionBattlefield.position, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 0)
		#if !tween.is_active():
# warning-ignore:return_value_discarded
		tween.start()
		emit_signal("transition_started");
		is_on_battlefield = true;
			
	
func enter_sickbay(body):
	if !is_on_battlefield:
		return;
	
	if body is Player:
# warning-ignore:return_value_discarded
		tween.interpolate_property(camera, "position", null, positionSickbay.position, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 0)
		#if !tween.is_active():
# warning-ignore:return_value_discarded
		tween.start()
		emit_signal("transition_started");
		is_on_battlefield = false;


func _on_Tween_tween_all_completed():
	emit_signal("transition_ended");
