extends KinematicBody2D

class_name Soldier

signal restored(soldier)
signal killed(soldier)

var bar_green = preload("res://Art/Health-Healing.png")
var bar_red = preload("res://Art/Health-Full.png")

var get_simple_path: FuncRef
var path: PoolVector2Array
var screen_size: Vector2
var wounded := false
var killed := false
var health := 100 setget _set_health
var shield := 0
var is_carried := false
var is_treated := false setget _set_is_treated
var restored := false
var is_bandaged := false
var carry_position : Node2D
var wait_for_kill = false;
export (PackedScene) var Shield;
export var speed:= 25
export var debug:= false
export var soldier_name := "Johnny" setget _set_soldier_name
export var veteran_level := 0
export (PackedScene) var BloodPool
onready var movementTimer: Timer = $MovementTimer
onready var healingTimer: Timer = $HealingTimer
onready var damageTimer: Timer = $DamageTimer
onready var debugLine: Line2D = $GlobalPosition/DebugLine
onready var healthBar: TextureProgress = $HealthBarHolder/HealthBar
onready var animationTree: AnimationTree = $AnimationTree;
onready var bloodParticles: Particles2D = $BloodParticles;
onready var healingParticles: Particles2D = $HealingParticles;
onready var playback = animationTree.get("parameters/playback")
var shooting = false;

func _ready():
	$Sprite.material = $Sprite.material.duplicate();
	screen_size = get_viewport_rect().size;
	debugLine.visible = debug;
	healthBar.visible = false;
	_update_label();
	$ShootingTimer.wait_time = randf() * 10+5;
	

func setup(get_simple_path_func: FuncRef):
	get_simple_path = get_simple_path_func;
	movementTimer.wait_time = (randf() * 5)+1;
	movementTimer.start();
	$ShootingTimer.start();
	shield = veteran_level;
	
	get_new_path();
	playback.start("Idle");

func ready_for_wave():
	get_new_path();
	movementTimer.wait_time = (randf() * 5)+1;
	movementTimer.start();
	$ShootingTimer.start();
	shield = veteran_level;
	
	if $ShieldHolder/ShieldContainer.get_child_count() > 0:
		for i in range(0, $ShieldHolder/ShieldContainer.get_child_count()):
			get_child(i).queue_free()
			
	for i in veteran_level:
		$ShieldHolder/ShieldContainer.add_child(Shield.instance())
	
	restored = false;
	killed = false;
	wounded = false;
	health = 100;
	_update_label();
	playback.start("Idle");
	enable_collision();


func _on_ShootingTimer_timeout():
	if wounded or killed or restored:
		return;
	shooting = false if shooting else true;
	set_animation_parameters(false, false, shooting);
	set_animation_vector(Vector2(0,-1));
	$ShootingTimer.wait_time = randf() * 10+5;
	
func _on_MovementTimer_timeout():
	get_new_path();
	
func get_new_path():
	var random_position = Vector2(screen_size.x*randf(), screen_size.y*randf());
	if restored:
		random_position = Vector2(screen_size.x*randf(), screen_size.y+(screen_size.y*randf()));
	if get_simple_path:
		path = get_simple_path.call_func(position, random_position);
	debugLine.points = path;
	
func _physics_process(delta):
	
	if is_carried:
		global_position = carry_position.global_position
		return
		
	if wait_for_kill:
		return;
	
	if wounded:
		return;
		
	if shooting:
		return;
		
	enable_collision();
	# Calculate the movement distance for this frame
	var distance_to_walk = speed * delta
	if restored: 
		distance_to_walk *= 0.3
	
	# Move the player along the path until he has run out of movement or the path ends.
	var moved = false;
	while distance_to_walk > 0 and path.size() > 0:
		moved = true;
		var distance_to_next_point = position.distance_to(path[0])
		if distance_to_walk <= distance_to_next_point:
			# The player does not have enough movement left to get to the next point.
			var direction = position.direction_to(path[0]);
			set_animation_vector(direction);
			position += direction * distance_to_walk
		else:
			# The player get to the next point
			position = path[0]
			path.remove(0)
		# Update the distance to walk
		distance_to_walk -= distance_to_next_point
	set_animation_parameters(moved, !moved, false)
		
func hit_by_bullet():
	if killed: 
		return
	if wounded:
		self.health -= 20;
	if killed: 
		return
	if shield > 0:
		shield -= 1;
		return;
	if !wounded:
		wait_for_kill = false;
		playback.start("Wounded");
		movementTimer.stop();
		wounded = true;
		AudioPlayer.play_sounds(randi() % 3 + LDEnums.Sounds.CALLOUT1, 5)
		healthBar.visible = true;
		disable_collision()
		damageTimer.wait_time = rand_range(0.1, 0.3);	
		damageTimer.start();
		bloodParticles.amount = int(bloodParticles.lifetime / damageTimer.wait_time);
		bloodParticles.emitting = true;
		yield(get_tree().create_timer(0.01), "timeout")
		enable_collision()
	else:
		var wait_time = damageTimer.wait_time;
		wait_time-= rand_range(0.05, 0.15);	
		damageTimer.wait_time = clamp(wait_time, 0.08, 0.6);	
	_update_label();
	
#	if killed or wounded:
#		var blood_pool: Node2D = BloodPool.instance();
#		self.get_parent().get_parent()
#		blood_pool.position = self.position
		

func set_animation_vector(direction: Vector2):
	if restored:
		direction *= 0.5
	animationTree.set("parameters/Idle/blend_position", direction);
	animationTree.set("parameters/Locomotion/blend_position", direction);
	animationTree.set("parameters/Death/blend_position", direction);
	animationTree.set("parameters/Wounded/blend_position", direction);
	
func set_animation_parameters(moving, idle, _shooting):
	if animationTree:
		animationTree.set("parameters/conditions/shooting", _shooting);
		animationTree.set("parameters/conditions/moving", moving);
		animationTree.set("parameters/conditions/idle", idle);


func _on_DamageTimer_timeout():
	self.health -= 1;

func _on_HealingTimer_timeout():
	if !is_treated:
		return;
	self.health += 1;
	if health == healthBar.max_value:
		restore()
	
func _set_soldier_name(newvalue):
	soldier_name = newvalue;
	_update_label();
	$Label.text = soldier_name
	
func _set_health(newvalue):
	health = newvalue;
	healthBar.value = health;
	killed = health <= 0;
	if killed:
		damageTimer.stop();
		bloodParticles.emitting = false;
		healthBar.visible = false;
		emit_signal("killed", self);
		playback.start("Death");
		$Sprite.material.set_shader_param("outline_width", 0.0);
		_end_interaction()
		disable_collision()
		
		_update_label();

func _update_label():
	var text = soldier_name;
	# text += "★".repeat(veteran_level);
	if killed:
		pass
		$Label.visible = false
	elif is_treated:
		$Label.add_color_override("font_color", Color("33ff6d"))
	elif wounded:
		pass
#		$Label.add_color_override("font_color", Color("ff000d"))
#		text += " (Wounded)"
	else:
		if $Label.has_color_override("font_color"):
			$Label.add_color_override("font_color", Color("ffffff"))
		
	$Label.text = text;
	
func interact(player: Player, action_type: int):
	if action_type == LDEnums.ActionType.PICK:
		if !wounded:
			return
		
		carry_position = player.get_carry_position()
		if !carry_position:
			return
		
		is_carried = true
		player.set_carried_interactible(self)
# warning-ignore:return_value_discarded
		self.connect("killed", player, "soldier_died", [], CONNECT_ONESHOT);
		player.is_interacting = true
		
		disable_collision()	
		global_position = carry_position.global_position
	elif action_type == LDEnums.ActionType.MORPHEUM:
		if !wounded:
			return
			
		health = int(clamp(health + healthBar.max_value * player.morphium_heal_factor, health, healthBar.max_value))
		player.morphium -= 1
		AudioPlayer.play_sounds(randi() % 3 + LDEnums.Sounds.MORPHIUM1, 6)
	elif action_type == LDEnums.ActionType.BANDAGE:
		damageTimer.wait_time *= 3
		is_bandaged = true
		healingParticles.emitting = true;
		AudioPlayer.play_sounds(LDEnums.Sounds.BANDAGE)
		$BandageTimer.start();
		
	
	
func end_interaction(player: Player):
	_end_interaction()
	
	if player:
		player.set_carried_interactible(null)
	
func _end_interaction():
	is_carried = false
	enable_collision();
	

func can_interact(_player: Player, action_type: int = LDEnums.ActionType.PICK)->bool:
	if action_type == LDEnums.ActionType.PICK || LDEnums.ActionType.MORPHEUM:
		return wounded && !killed && !_player.get_carried_interactible()
	elif action_type == LDEnums.ActionType.BANDAGE:
		return !is_bandaged
		
	return false
	
func restore():
	wounded = false
	killed = false
	healthBar.visible = false
	damageTimer.stop()
	enable_collision()
	_update_label()
	restored = true
	is_bandaged = false
	emit_signal("restored", self)
	playback.start("Idle");
	set_animation_parameters(false, true, false);
	set_animation_vector(Vector2(1,0));
	get_new_path();
	movementTimer.start();

func _set_is_treated(new_value):
	is_treated = new_value
	if is_treated:
		healthBar.texture_progress = bar_green
		bloodParticles.emitting = false;
		healingParticles.emitting = true;
		healingTimer.start();
		damageTimer.stop();
	else:
		healthBar.texture_progress = bar_red
		healingParticles.emitting = false;
		if wounded:
			damageTimer.start();
		healingTimer.stop();
		
	_update_label()


func _on_bandageTimer_timeout():
	healingParticles.emitting = false;

func _on_Area2D_area_entered(area):
	if !area.is_in_group("PlayerArea"):
		return
	if !wounded or killed:
		return;
	$Sprite.material.set_shader_param("outline_width", 0.3);


func _on_Area2D_area_exited(area):
	if !area.is_in_group("PlayerArea"):
		return
	$Sprite.material.set_shader_param("outline_width", 0.0);
	
func enable_collision():
	$Area2D/CollisionShape2D.set_deferred("disabled", false)
	$CollisionShape2D.set_deferred("disabled", false)
	
func disable_collision():
	$Area2D/CollisionShape2D.set_deferred("disabled", true)
	$CollisionShape2D.set_deferred("disabled", true)


