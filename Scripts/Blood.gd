extends Node2D

export var pooling_time = 5
onready var animated_sprite := $AnimatedSprite
onready var timer := $Timer
onready var animations := [
	"Blood1",
	"Blood2",
	"Blood3",
	"Blood4"
]


func _ready():
	var animation_index = randi() % animations.size()
	var animation = animations[animation_index]
	
	animated_sprite.frame = 0
	animated_sprite.animation = animation
	animated_sprite.playing = true


func _on_AnimatedSprite_animation_finished():
	timer.start(pooling_time)


func _on_Timer_timeout():
	self.queue_free()
